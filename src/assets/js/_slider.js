// インスタンス : dragSlider / 1_init
// galleryページ > 画像スライダー
//=============================================================

export class Slider {

  constructor() {

    this.slides = [];
    this.onDestroy();
    this.$wrap = document.querySelectorAll('.slide-wrap');

    if (this.$wrap.length && (PC || __WW__ > tabletStyle)) {

      for (var i = 0; i < this.$wrap.length; i++) {

        var ease = Number(this.$wrap[i].dataset.ease) ? Number(this.$wrap[i].dataset.ease) : 0.1;

        this.slides.push({

          // elememnt
          $wrap: this.$wrap[i],
          $body: this.$wrap[i].querySelector('.slide-body'),
          $slide: this.$wrap[i].querySelectorAll('.slide'),
          $progress: this.$wrap[i].querySelector('.progress'),

          // width
          type: this.$wrap[i].dataset.type,
          padding: Number(this.$wrap[i].dataset.padding),
          margin: Number(this.$wrap[i].dataset.margin),
          total: 0,
          max: 0,
          min: 0,

          // position
          ease: ease,
          anim: {
            x: 0,
            progress: 0,
          },
          start: {
            enable: false,
            x: 0,
          },
          ing: {
            x: 0,
          },
          end: {
            x: 0,
          },

          // events
          touchstart: null,
          touchmove: null,
          touchend: null,
          mousedown: null,
          mousemove: null,
          mouseleave: null,
          mouseup: null,

        });

      }

      this.onResize();
      this.addTouchEvents();
      this.addMouseEvents();

    }

  }

  onResize() {

    for (var i = 0; i < this.slides.length; i++) {

      var v = this.slides[i];

      var padding = v.padding;
      var margin = v.margin;
      if (v.type === 'ratio') {
        padding = window.innerWidth * v.padding;
        margin = window.innerWidth * v.margin;
      }

      v.total = 0;
      for (var n = 0; n < v.$slide.length; n++) {
        var rect = v.$slide[n].getBoundingClientRect();
        v.$slide[n].style.margin = `0px ${padding/2}px`;
        v.total += rect.width + padding;
      }
      v.total = v.total + margin / 2 + padding;
      v.max = v.total - window.innerWidth;
      v.$body.style.margin = `0px ${ margin/2 + padding/2 }px`;
      v.$body.style.width = v.total + 'px';

    }

  }

  addTouchEvents() {

    //
    this.slides.forEach((v, i) => {

      v.touchstart = function(e) {
        if (v.total > window.innerWidth) {
          v.start.enable = true;
          v.$wrap.classList.add('is-dragging');
          v.start.x = e.clientX;
          if (e.touches && e.touches[0]) {
            v.start.x = e.touches[0].clientX;
          }
        }
      };
      v.touchmove = function(e) {
        if (v.start.enable) {

          var x = e.clientX;
          if (e.touches && e.touches[0]) {
            x = e.touches[0].clientX;
          } else if (e.originalEvent && e.originalEvent.changedTouches[0]) {
            x = e.originalEvent.changedTouches[0].clientX;
          }

          var _x = v.end.x + (x - v.start.x);
          v.ing.x = (_x > v.min) ? v.min : (_x < -v.max) ? -v.max : _x;

        }
      };
      v.touchend = function(e) {
        v.start.enable = false;
        v.$wrap.classList.remove('is-dragging');
        v.end.x = v.ing.x;
      };
      v.$wrap.addEventListener('touchstart', v.touchstart, { passive: true });
      v.$wrap.addEventListener('touchmove', v.touchmove, { passive: true });
      v.$wrap.addEventListener('touchend', v.touchend);

    });

  }

  addMouseEvents() {

    //
    this.slides.forEach((v, i) => {

      v.mousedown = function(e) {
        if (v.total > window.innerWidth) {
          v.start.enable = true;
          v.$wrap.classList.add('is-dragging');
          v.start.x = e.clientX;
        }
      };
      v.mousemove = function(e) {
        if (v.start.enable) {
          var x = v.end.x + (e.clientX - v.start.x);
          v.ing.x = (x > v.min) ? v.min : (x < -v.max) ? -v.max : x;
        }
      };
      v.mouseup = function(e) {
        v.start.enable = false;
        v.$wrap.classList.remove('is-dragging');
        v.end.x = v.ing.x;
      };
      v.mouseleave = function(e) {
        v.start.enable = false;
        v.$wrap.classList.remove('is-dragging');
        v.end.x = v.ing.x;
      };
      v.$wrap.addEventListener('mousedown', v.mousedown);
      v.$wrap.addEventListener('mousemove', v.mousemove);
      v.$wrap.addEventListener('mouseup', v.mouseup);
      v.$wrap.addEventListener('mouseleave', v.mouseleave);

    });

  }

  onDestroy() {
    this.slides.forEach((v, i) => {
      v.$wrap.removeEventListener('touchstart', v.touchstart, { passive: true });
      v.$wrap.removeEventListener('touchmove', v.touchmove, { passive: true });
      v.$wrap.removeEventListener('touchend', v.touchend);
      v.$wrap.removeEventListener('mousedown', v.mousedown);
      v.$wrap.removeEventListener('mousemove', v.mousemove);
      v.$wrap.removeEventListener('mouseup', v.mouseup);
      v.$wrap.removeEventListener('mouseleave', v.mouseleave);
    });
    this.slides = [];
  }

  onUpdate() {
    for (var i = 0; i < this.slides.length; i++) {
      var v = this.slides[i];

      v.anim.x += (v.ing.x - v.anim.x) * v.ease;
      v.anim.progress = Math.round(Math.abs(v.anim.x / v.max * 100));
      v.$body.style.transform = `translate3d(${v.anim.x}px, 0px, 0px )`;

      if (v.$progress) v.$progress.innerText = v.anim.progress;

    }
  }


}
